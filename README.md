# COMS12200 2019-20, Part 2 : From Digital Logic to Computer Processors

This is the material to be used for the second part of the course.

## TimeTable

| Week |Date| Lecture Slot | Date | Lab. Slot | Date | Lecture slot |  |
|--|--|--|--|--|--|--|--|
| 9 | 25/11/19 | Data, Control, & Instructions | 27/11/19 | Build-a-comp boards and simulator | 29/11/19 | Intro to Memory |  |
| 10 | 02/12/19 | Execution Cycle & Control Flow | 04/12/19 | Arithmetic | 06/12/19 | Processor Control Flow (part 2) & Register Machine |  |
| 11 | 09/12/19 | State Machines, Decoding | 11/12/19 | Decoding | 13/12/19 |  Memory Paradigms |  |
| 12 | 16/12/19 | Viva Topics & Revision | 18/12/19 | Revision Week | 20/12/19 | Revision Week |  |
| 13 | 27/01/20 | Counter Machines | 29/01/20 | Build Counter Machine Part 1 | 31/01/20 | Memory Hierarchy |  |
| 14 | 03/02/20 | Instructions Set Architectures | 05/02/20 | Build Counter Machine Part 2 | 07/02/20 | An Example ISA (ARM Thumb V1) |  |
| 15 | 10/02/20 | Continuing "An Example ISA" | 12/02/20 | Build Counter Machine Part 3 | 14/02/20 | Assemblers & Assembly Language |  |
| 16 | 17/02/20 | Pending | 19/02/20 | Pending | 21/02/20 | David May |  |



## Labs info

The computer architecture unit for computer science students at the University of Bristol is taught using a set of basic hardware boards, called as **Build-a-comp** modules. The students learn about their inner workings and then use them to create bigger computing circuits.  Information about the modules (an informal specification) and how they can be used can be found on [this page](https://teachingtechnologistbeth.github.io/ModuleSim/modules.html). Please, familiarize your self with them.

While the hardware modules will be demonstrated in the lab, for practical reasons, we will be using a custom-made simulator of the modules to complete our circuits.

You can download the latest version's runnable jar file from the release page  [here](https://github.com/uobteachingtechnologist/ModuleSim/releases).

To run it, you must have java installed on your computer. You can either launch it via the command line/terminal using:  

    java -jar whatever_you_called_the_downloaded_file.jar 

or in your file explorer, you can just double click on the file and select open with java in the context menu.
